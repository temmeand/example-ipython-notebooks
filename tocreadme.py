#!/usr/bin/env python

# Adapted from
# https://bitbucket.org/hrojas/learn-pandas/raw/bea538e1c7e6d6cfe25a9f88dc515eb1e61e8676/gen-readme.py

from sys import version_info

if version_info[0] < 3:
    from urllib import quote
else:
    from urllib.request import quote


from glob import glob
import json

header = '''List of Example Notebooks
=========================
'''



format_item = '* [{name}]({url})'.format
bb_url = 'bitbucket.org/temmeand/example-ipython-notebooks/raw/master/{}'.format


def notebooks():
    return glob('*.ipynb')


def lesson_name(filename):
    # with open(filename) as fo:
    #     return json.load(fo)['metadata']['name']
    return filename[:-6]


def nb_url(filename):
    # The double quote is not an error
    raw_url = bb_url(quote(quote(filename)))
    return 'http://nbviewer.ipython.org/urls/{}'.format(raw_url)


def write_readme(nblist, fo):
    fo.write('{}\n'.format(header))
    for nb in nblist:
        name = lesson_name(nb)
        url = nb_url(nb)
        fo.write('{}\n'.format(format_item(name=name, url=url)))


def main():
    nblist = notebooks()
    text = []
    with open('README.md', 'r') as fi:
        for line in fi:
            if "List of Example Notebooks" not in line:
                text.append(line)
            else:
                break

    with open('README.md', 'w') as fo:
        fo.writelines(text)
        write_readme(nblist, fo)


if __name__ == '__main__':
    main()

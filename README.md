Example IPython Notebooks
=========================

This is a collection of example IPython notebooks. Most have been written or put
together by me. Please feel free to re-use with attribution. 

Repo Availability/Locations
---------------------------

This repository is mirrored on https://bitbucket.org/temmeand/ and
https://gitlab.msu.edu/u/temmeand.

Errors/Problems
---------------

Please contact me or submit a pull request if you find an error. Thank you.

List of Example Notebooks
=========================

* [pozar-rf-design-example-6-1](http://nbviewer.ipython.org/urls/bitbucket.org/temmeand/example-ipython-notebooks/raw/master/pozar-rf-design-example-6-1.ipynb)
* [MSU-supercomputer-HFSS-and-IPython](http://nbviewer.ipython.org/urls/bitbucket.org/temmeand/example-ipython-notebooks/raw/master/MSU-supercomputer-HFSS-and-IPython.ipynb)
* [gating](http://nbviewer.ipython.org/urls/bitbucket.org/temmeand/example-ipython-notebooks/raw/master/gating.ipynb)
* [discrete-fourier-transform-numpy](http://nbviewer.ipython.org/urls/bitbucket.org/temmeand/example-ipython-notebooks/raw/master/discrete-fourier-transform-numpy.ipynb)
